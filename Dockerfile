FROM python:slim as deps

SHELL ["/bin/bash", "-c"]
WORKDIR /app/build

ENV PIP_CACHE_DIR "/app/build/.cache/pip"

COPY . .
RUN pip install -r all_requirements.txt

FROM python:slim
COPY --from=0 /usr/local/ /usr/local
