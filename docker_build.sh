#!/bin/bash
set -x
shopt -s globstar

mkdir -p build/wheels
cd build

# Merge all requirements into one file
for file in ../**/requirements.txt; do
  while read -r line; do
    # If line is a filename, copy file to wheels dir and write new filename
    if [[ $line == *"/"* ]]; then
      name="wheels/$(basename "$line")"
      cp -n "$(dirname $file)/$line" "$name"
      echo "$name"
    # If line is non-empty and no comment, print in lower case
    elif [[ $line =~ [^[:space:]] ]] && [[ $line != "#"* ]]; then
      echo "${line,,}"
    fi
  done < "$file"
done |

  # Remove duplicates and sort according to version number
  sort -V -u |
  # Only use latest version
  # Uses multiline functionality: https://edu.nl/t6hev
  sed -E ':begin;$!N;s/([^><=]*)[><=]+.*\n\1([><=]+.*)/\1\2/;tbegin;P;D' > all_requirements.txt

[ -z "$CI_REGISTRY_IMAGE" ] && CI_REGISTRY_IMAGE="registry.gitlab.com/jager567/ci-testing"
[ -z "$CI_COMMIT_BRANCH" ] && CI_COMMIT_BRANCH="$CI_DEFAULT_BRANCH"

export DOCKER_BUILDKIT=1

# Dockerfile is one dir above, because we're in build dir
# This is done to reduce the size of the build context.
docker build \
    --progress=plain \
    -f ../Dockerfile \
    --rm=false \
    -t "$CI_REGISTRY_IMAGE/python:$CI_COMMIT_BRANCH" \
    --build-arg BUILDKIT_INLINE_CACHE=1 \
    --cache-from "$CI_REGISTRY_IMAGE/python:$CI_COMMIT_BRANCH" \
    .
